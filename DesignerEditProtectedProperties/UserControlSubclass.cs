﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DesignerEditProtectedProperties
{
    public partial class UserControlSubclass : UserControl
    {
        [Browsable(true)]
        protected bool MyDesignerInaccessibleProperty { get; set; }

        public bool MyDesignerAccessibleProperty { get; set; }

        public UserControlSubclass()
        {
            InitializeComponent();
        }
    }
}
